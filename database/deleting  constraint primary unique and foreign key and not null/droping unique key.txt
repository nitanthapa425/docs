aggg
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| age   | int          | NO   |     | NULL    |       |
| name  | varchar(100) | YES  | UNI | NULL    |       |
+-------+--------------+------+-----+---------+-------+

syntax
ALTER TABLE aggg DROP INDEX index_name

to get index_name
SHOW INDEX FROM aggg;                 not3 not select

alter table aggg drop index name; 

+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| age   | int          | NO   |     | NULL    |       |
| name  | varchar(100) | YES  |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+