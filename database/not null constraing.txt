not null constraint

CREATE TABLE table(name VARCHAR(100) N0T NULL, age int NOT NULL);

if any column is make not null
then it does not contain null
and we know any column contain null by default
but since the column is make not null
thus 
for int column it contain 0 by default
for varchar column it contain "" ie blank by default

