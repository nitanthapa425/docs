limit always write at last of select 
and note it perfom action at last

empdetails
+----+--------+--------+
| id | name   | salary |
+----+--------+--------+
|  1 | nitan  |    100 |
|  2 | utshab |    200 |
|  3 | roshan |    300 |
|  4 | kamal  |    400 |
|  5 | dilly  |    500 |
|  6 | madhav |    600 |
+----+--------+--------+

select*from empdetails limit 3;
+----+--------+--------+
| id | name   | salary |
+----+--------+--------+
|  1 | nitan  |    100 |
|  2 | utshab |    200 |
|  3 | roshan |    300 |
+----+--------+--------+

select max(salary) from empdetails limit 3;..........note you might be thinking it will print 300
but it print 600 because limit perform action at last 
first max(salary) operation then limit operation is perform
+-------------+
| max(salary) |
+-------------+
|         600 |
+-------------+
